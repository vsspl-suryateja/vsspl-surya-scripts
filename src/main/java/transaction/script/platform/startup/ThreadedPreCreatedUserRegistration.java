package transaction.script.platform.startup;

import java.util.concurrent.TimeUnit;

public class ThreadedPreCreatedUserRegistration implements Runnable {
	public void run() {
		System.out.println(Thread.currentThread().getName());
		try {
			TimeUnit.MILLISECONDS.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Super machi " + Thread.currentThread().getName());
	}

	public static void main(String[] args) throws InterruptedException {
		ThreadedPreCreatedUserRegistration classObj = new ThreadedPreCreatedUserRegistration();
		Thread thread = null;
		for (int j = 0; j < 5; j++) {
			System.out.println("started next set");
			for (int i = 1; i < 6; i++) {
				thread = new Thread(classObj, "abhi's thread*" + i + "," + j);
				thread.start();
			}
			while (thread.isAlive()) {
				TimeUnit.MILLISECONDS.sleep(200);
				System.out.println("Still alive");
			}
		}
	}

	public static void main2(String[] args) throws InterruptedException {
		ThreadedPreCreatedUserRegistration classObj = new ThreadedPreCreatedUserRegistration();
		for (int i = 0; i < 5; i++) {
			Thread thread1 = new Thread(classObj, "abhi's thread*" + i);
			thread1.start();
			Thread thread2 = new Thread(classObj, "abhi's thread**" + i);
			thread2.start();
			Thread thread3 = new Thread(classObj, "abhi's thread***" + i);
			thread3.start();
			while (thread1.isAlive()) {
				//TimeUnit.MILLISECONDS.sleep(50);
				//System.out.println("thread1 is alive");
			}
			while (thread2.isAlive()) {
				//TimeUnit.MILLISECONDS.sleep(50);
				//System.out.println("thread2 is alive");
			}
			while (thread3.isAlive()) {
				//TimeUnit.MILLISECONDS.sleep(50);
				//System.out.println("thread3 is alive");
			}
			System.out.println("finished");
			/*
						System.out.println(thread1.getPriority());
						System.out.println(thread1.getName());
						System.out.println(thread1.isAlive());
						System.out.println(thread1.isDaemon());
						System.out.println(thread1.isInterrupted());
						System.out.println(thread1.getState());
						System.out.println(thread1.getThreadGroup());
						System.out.println(Thread.MAX_PRIORITY);
						System.out.println(Thread.MIN_PRIORITY);
						System.out.println(Thread.NORM_PRIORITY);
						System.out.println(Thread.activeCount());
						System.out.println(Thread.currentThread());
			*/
		}
	}
}