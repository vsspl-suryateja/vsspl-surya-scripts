/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import transaction.script.constants.AppConstants;

public class DynamoConstants {

	public static String DYNAMO = null;
	public static String PROFILE = null;
	public static String CRAWLED_CONTENT = null;
	public static String ACCOUNTS = null;
	public static String TRADERAPP = null;
	public static String BROKERAPP = null;
	public static String FARMERAPP = null;
	public static String CONNECTS = null;
	public static String CONTENT = null;
	public static String CCP = null;
	public static String KALGUDI_AUTH = null;
	public static String APP_VERSION = null;
	public static String RESTRICT_NUMBERS = null;
	public static String AGRIINPUTAPP = null;
	public static String TRANSPORTERAPP = null;
	public static String WAREHOUSEAPP = null;
	public static String COSAPP = null;

	public static String value = AppConstants.DYNAMO_ENVIRONMENT;
	static {
		if (value.equals("aws")) {
			DynamoConstants.aws();
		} else if (value.equals("staging")) {
			DynamoConstants.staging();
		} else if (value.equals("local")) {
			DynamoConstants.local();
		} else {
			DynamoConstants.cde();
		}

	}

	public static void staging() {

		DYNAMO = "rckalgudi";
		PROFILE = "staging_profile";
		CRAWLED_CONTENT = "staging_crawled_content";
		ACCOUNTS = "staging_accounts";
		TRADERAPP = "staging_traderapp";
		BROKERAPP = "staging_brokerapp";
		FARMERAPP = "staging_farmerapp";
		CONNECTS = "staging_connects";
		CONTENT = "staging_content";
		CCP = "staging_ccp";
		KALGUDI_AUTH = "staging_kalgudi_auth";
		APP_VERSION = "staging_app_version";
		RESTRICT_NUMBERS = "staging_restrict_numbers";
		AGRIINPUTAPP = "staging_agriinput";
		TRANSPORTERAPP = "staging_transporter";
		WAREHOUSEAPP = "staging_warehouse";
		COSAPP = "staging_cos";

	}

	public static void aws() {

		DYNAMO = "production";
		PROFILE = "aws_profile";
		CRAWLED_CONTENT = "aws_crawled_content";
		ACCOUNTS = "aws_accounts";
		TRADERAPP = "aws_traderapp";
		BROKERAPP = "aws_brokerapp";
		FARMERAPP = "aws_farmerapp";
		CONNECTS = "aws_connects";
		CONTENT = "aws_content";
		CCP = "aws_ccp";
		KALGUDI_AUTH = "aws_kalgudi_auth";
		APP_VERSION = "staging_app_version";
		RESTRICT_NUMBERS = "aws_restrict_numbers";
		AGRIINPUTAPP = "aws_agriinput";
		TRANSPORTERAPP = "aws_transporter";
		WAREHOUSEAPP = "aws_warehouse";
		COSAPP = "aws_cos";

	}

	public static void local() {
		DYNAMO = "devkalgudi";
		PROFILE = "local_profile";
		CRAWLED_CONTENT = "local_crawled_content";
		ACCOUNTS = "local_accounts";
		TRADERAPP = "local_traderapp";
		BROKERAPP = "local_brokerapp";
		FARMERAPP = "local_farmerapp";
		CONNECTS = "local_connects";
		CONTENT = "local_content";
		CCP = "local_ccp";
		KALGUDI_AUTH = "local_kalgudi_auth";
		APP_VERSION = "staging_app_version";
		RESTRICT_NUMBERS = "local_restrict_numbers";
		AGRIINPUTAPP = "local_agriinput";
		TRANSPORTERAPP = "local_transporter";
		WAREHOUSEAPP = "local_warehouse";
		COSAPP = "local_cos";

	}

	public static void cde() {
		DYNAMO = "cde";
		PROFILE = "profile";
		CRAWLED_CONTENT = "crawled_content";
		ACCOUNTS = "accounts";
		TRADERAPP = "traderapp";
		BROKERAPP = "brokerapp";
		FARMERAPP = "farmerapp";
		CONNECTS = "connects";
		CONTENT = "content";
		CCP = "ccp";
		KALGUDI_AUTH = "kalgudi_auth";
		APP_VERSION = "app_version";
		RESTRICT_NUMBERS = "restrict_numbers";
		AGRIINPUTAPP = "agriinput";
		TRANSPORTERAPP = "transporter";
		WAREHOUSEAPP = "warehouse";
		COSAPP = "cosapp";

	}
}
