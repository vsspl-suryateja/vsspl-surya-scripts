package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Ec21_java {
	public static void main(String[] args) {
		String baseUrl = "company_info.html";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <= 13; i++) {
			try {

				Document document = Jsoup.parse(IOUtils.toString(new URL("http://supplier.ec21.com/Agriculture--01/1/exporters/page_"+i+".html?countryCd=RU")));
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;
			
					String  products = "", address = "", mobile = "", Area = "", bizName = "", website = "",bizType ="",contact_person="",fax ="";
					
					Document linkDocument = null;
					try {
						Elements item_list = document.select("div.Item_list_tx2");
						for (Element spec : item_list) {
							Row row = sheet.createRow(rowNumber);
							rowNumber++;
							   Elements dl = spec.select("dl.item_ls_lf");
					            Elements dts = dl.select("dt");
					            Elements h2 = dl.select("h2");
					            Elements anchor= h2.select("a");
					            String href = anchor.attr("href");
						
						try {
							linkDocument = Jsoup.parse(IOUtils.toString(new URL(href+baseUrl)));
						} catch (Exception e1) {
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						Elements elements = linkDocument.getElementsByClass("script_box3");
						for (Element test :elements)
						{
							for(Element element2 :test.getElementsByClass("t_itc3"))
							{
								String TotalData = element2.getElementsByClass("title").text();
								
							if(TotalData.contains("Company Name"))	
							{
								bizName =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Location"))	
							{
								Area =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Business Type"))	
							{
								bizType =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Website"))	
							{
								website =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Phone"))	
							{
								mobile =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Fax"))	
							{
								fax =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Contact"))	
							{
								contact_person =element2.getElementsByClass("txt").text();
							}
							else if(TotalData.contains("Address"))	
							{
								address =element2.getElementsByClass("txt").text();
							}else if(TotalData.contains("Keyword"))	
							{
								products =element2.getElementsByClass("txt").text();
							}
								
							}
						}
						index++;
						System.out.println("Row :"+index+" Page:"+i);
						row.createCell(0).setCellValue(bizName);
						row.createCell(1).setCellValue(contact_person);
						row.createCell(2).setCellValue(bizType);
						row.createCell(3).setCellValue(mobile);
						row.createCell(4).setCellValue(products);
						row.createCell(5).setCellValue(address);
						row.createCell(6).setCellValue(website);
						row.createCell(7).setCellValue(fax);
						row.createCell(8).setCellValue(Area);
}
					
						
					} catch (IndexOutOfBoundsException e) {
						break;
					}
					
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/nandini/Desktop/Corpus/Russia/Exporters_EC21.xlsx"))) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		}
	}