package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class EuroPages {
	public static void main(String[] args) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <= 4; i++) {
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL("http://agriculture-livestock.europages.co.uk/companies/Ukraine/pg-"+i+"/Agriculture%20-%20import-export.html")));
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;
				while (true) {
					String businessRange = "", products = "Agriculture", address = "", mobile = "", otherMobile = "", bizName = "", website = "",country="";
					Row row = sheet.createRow(rowNumber);
					rowNumber++;
					try {
						String text = document.getElementsByClass("company-name").get(index).attr("href");
						Document linkDocument = null;
						try {
							linkDocument = Jsoup.parse(IOUtils.toString(new URL(text)));
						} catch (Exception e1) {
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						bizName = linkDocument.getElementsByClass("comp").text();
						//Address
						address =linkDocument.getElementsByAttributeValue("itemprop", "addressLocality").text();
						//addressCountry
						country =linkDocument.getElementsByAttributeValue("itemprop", "addressCountry").text();
						//info-tel-num
						mobile =linkDocument.getElementsByClass("info-tel-num").text();
						index++;
						System.out.println("row number "+index+" page: "+i);
					} catch (IndexOutOfBoundsException e) {
						break;
					}
					row.createCell(0).setCellValue(bizName);
					row.createCell(1).setCellValue(mobile);
					if (products.isEmpty())
						row.createCell(2).setCellValue(businessRange);
					else
					row.createCell(2).setCellValue(products);
					row.createCell(3).setCellValue(address);
					row.createCell(4).setCellValue(country);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/nandini/Desktop/Corpus/Ukraine/Agriculture_Euro.xlsx"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}